#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glaux.lib")						// Added for texture mapping

#include "main.h"
#include "Camera.h"

CCamera g_Camera;										// This is our global camera object

float aSpeed;											// Actual camera speed

bool  g_bFullScreen = true;								// Set full screen as default
HWND  g_hWnd;											// This is the handle for the window
RECT  g_rRect;											// This holds the window dimensions
HDC   g_hDC;											// General HDC - (handle to device context)
HGLRC g_hRC;											// General OpenGL_DC - Our Rendering Context for OpenGL
HINSTANCE g_hInstance;									// This holds the global hInstance for UnregisterClass() in DeInit()

#define GROUND_ID	6

UINT g_Texture[MAX_TEXTURES] = {0};		//1 konflikt				// This holds the texture info, referenced by an ID

bool ruch = false;
bool rucha = false;

bool light0 = true;
bool light1 = true;

float MAX2f(float a, float b)
{
	if(a > b)
		return a;
	if(b > a)
		return b;
	else
		return a;
}
/////////////////////// TEST /////////////////////////

struct tablica15
{
	float x;
	float z;
};

#define N 121
struct tablica15 tablica[N];

bool colisiondetect(float x1, float z1, float radis1, float x2, float z2, float radis2)
{
	//if(x2==0.0f || z2==0.0f)
		//return true;

    float xd, zd, Distance;
     
    xd = x2-x1;
    zd = z2-z1;     
    Distance = sqrt(xd*xd + zd*zd);
     
     
    if( radis1 + radis2 >= Distance)
        return false; //collision
    return true;    //no collision    
}

bool kolizja()
{
	for(int i = 0; i < N; i++)
	{
		if(!colisiondetect(g_Camera.temp_m_vPosition.x, g_Camera.temp_m_vPosition.z, 1.0f, tablica[i].x, tablica[i].z, 2.0f))
			return false;
	}
	return true;
}

bool kolizja2()
{
	if(g_Camera.temp_m_vPosition.x > 50.0f || g_Camera.temp_m_vPosition.z > 50.0f || g_Camera.temp_m_vPosition.x < -50.0f || g_Camera.temp_m_vPosition.z < -50.0f)
		return false;
	else
		return true;
}

int e = 0;
///////////// KONIEC TESTU //////////////////

///////////////////////////////// INIT GAME WINDOW \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

void Init(HWND hWnd)
{
	short int k = 0;
	g_hWnd = hWnd;										// Assign the window handle to a global window handle
	GetClientRect(g_hWnd, &g_rRect);					// Assign the windows rectangle to a global RECT
	InitializeOpenGL(g_rRect.right, g_rRect.bottom);	// Init OpenGL with the global rect


	CreateTexture2(g_Texture, "Ground.bmp", GROUND_ID	);

	/// TEST COLISION ///
	for(int i = 0; i < N; i++)
	{
		tablica[i].x = 0.0f;
		tablica[i].z = 0.0f;
	}
	for(int i = -50; i < 60; i+=10)
	{
		for(int j = 50; j > -60; j-=10)
		{
			tablica[k].x = (float)i;
			tablica[k].z = (float)j;
			k++;
		}
	}

	// Init our camera position

						// Position        View		   Up Vector
	g_Camera.PositionCamera(0.0f, 1.5f, 6.0f,   0.0f, 1.5f, 0.0f,   0.0f, 1.0f, 0.0f);
}

///////////////////////////////// LOCK FRAME RATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

bool LockFrameRate(int frame_rate)
{
	static float lastTime = 0.0f;
	
	// Get current time in seconds (milliseconds * .001 = seconds)
	float currentTime = GetTickCount() * 0.001f; 

	// Get the elapsed time by subtracting the current time from the last time
	// If the desired frame rate amount of seconds has passed -- return true (ie Blit())
	if((currentTime - lastTime) > (1.0f / frame_rate))
	{
		// Reset the last time
		lastTime = currentTime;	
			return true;
	}

	return false;
}

///////////////////////////////// MAIN GAME LOOP \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

WPARAM MainLoop()
{
	MSG msg;

	while(1)											// Do our infinate loop
	{													// Check if there was a message
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
        { 
			if(msg.message == WM_QUIT)					// If the message wasnt to quit
				break;
            TranslateMessage(&msg);						// Find out what the message does
            DispatchMessage(&msg);						// Execute the message
        }
		else if(LockFrameRate(60))						// If it is time to render
		{ 
			g_Camera.Update();							// Update the camera information
			//GetKeys();
			KeyPressed();	
			RenderScene();								// Render the scene every frame
        } 
	}

	DeInit();											// Clean up and free all allocated memory

	return(msg.wParam);									// Return from the program
}

void GetKeys()
{
	if(GetKeyState('W') & 0x80)
		ruch = true;
	else
		ruch = false;

	if(GetKeyState(VK_SHIFT) & 0x80)
		rucha = true;
	else
		rucha = false;
}

void KeyPressed()
{
	if(GetKeyState(VK_SHIFT) & 0x80)
		aSpeed = runSpeed;
	else
		aSpeed = kSpeed;
	// Check if we hit the Up arrow or the 'w' key
	if(GetKeyState(VK_UP) & 0x80 || GetKeyState('W') & 0x80) {				

		// Move our camera forward by a positive SPEED
		g_Camera.CompMove(aSpeed);
		if(kolizja() && kolizja2())
			g_Camera.MoveCamera();
	}

	// Check if we hit the Down arrow or the 's' key
	if(GetKeyState(VK_DOWN) & 0x80 || GetKeyState('S') & 0x80) {			

		// Move our camera backward by a negative SPEED
		g_Camera.CompMove(-aSpeed);
		if(kolizja() && kolizja2())
			g_Camera.MoveCamera();				
	}

	// Check if we hit the Left arrow or the 'a' key
	if(GetKeyState(VK_LEFT) & 0x80 || GetKeyState('A') & 0x80) {			

		// Strafe the camera left
		g_Camera.CompStrafe(-aSpeed);
		if(kolizja() && kolizja2())
			g_Camera.StrafeCamera();
		}

	// Check if we hit the Right arrow or the 'd' key
	if(GetKeyState(VK_RIGHT) & 0x80 || GetKeyState('D') & 0x80) {			

		// Strafe the camera right
		g_Camera.CompStrafe(aSpeed);
		if(kolizja() && kolizja2())
			g_Camera.StrafeCamera();
	}

	if(GetKeyState('T') & 0x80)
	{
		if(light0)
		{
			g_Camera.m_vPosition;
			g_Camera.m_vView;
			light0 = false;
			glDisable(GL_LIGHT0);
		}
		else
		{
			light0 = true;
			glEnable(GL_LIGHT0);
		}
	}

	if(GetKeyState('Y') & 0x80)
	{
		if(light1)
		{
			g_Camera.m_vPosition;
			g_Camera.m_vView;
			light1 = false;
			glDisable(GL_LIGHT1);
		}
		else
		{
			light1 = true;
			glEnable(GL_LIGHT1);
		}
	}
}
void Draw3DSGrid()
{
	float z = 0.25f;
	//glColor3ub(0, 255, 0);
	glBindTexture(GL_TEXTURE_2D, g_Texture[GROUND_ID]);

	// Draw a 1x1 grid along the X and Z axis'
	for(float j = -50; j < 50; j=j+z)
	{
		for(float i = -50; i < 50; i=i+z)
		{
		// Start drawing some lines
		/*glBegin(GL_LINES);

			// Do the horizontal lines (along the X)
			glVertex3f(-50, 0, i);
			glVertex3f(50, 0, i);

			// Do the vertical lines (along the Z)
			glVertex3f(i, 0, -50);
			glVertex3f(i, 0, 50);

		// Stop drawing lines
		glEnd();*/

			glBegin(GL_QUADS);
				
				// Do the horizontal lines (along the X)
				glTexCoord2f(0.0f, 0.0f); glVertex3f(j, 0, i);
				glTexCoord2f(0.0f, 1.0f); glVertex3f((j+z), 0, i);
				glTexCoord2f(1.0f, 1.0f); glVertex3f((j+z), 0, (i+z));
				glTexCoord2f(1.0f, 0.0f); glVertex3f(j, 0, (i+z));

			// Stop drawing lines
			glEnd();
		}
	}
}

void DrawCube()
{
	float m1_amb[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	float m1_dif[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	float m1_spe[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 50.0f);

	int i, j;
	float size = 1.0f;
	glTranslatef(50.0f,1.05f,60.0f);

	for(i = 0;i < 11; i++)
	{
		glTranslatef(-110.0f,0.0f,-10.0f);
		for(j = 0; j < 11; j++)
		{
			glTranslatef(10.0f,0.0f,0.0f);
			glBegin(GL_QUADS);		// Draw The Cube Using quads
				glColor3f(0.0f,1.0f,0.0f);	// Color Blue
				glVertex3f( size, size,-size);	// Top Right Of The Quad (Top)
				glVertex3f(-size, size,-size);	// Top Left Of The Quad (Top)
				glVertex3f(-size, size, size);	// Bottom Left Of The Quad (Top)
				glVertex3f( size, size, size);	// Bottom Right Of The Quad (Top)

				glVertex3f( size,-size, size);	// Top Right Of The Quad (Bottom)
				glVertex3f(-size,-size, size);	// Top Left Of The Quad (Bottom)
				glVertex3f(-size,-size,-size);	// Bottom Left Of The Quad (Bottom)
				glVertex3f( size,-size,-size);	// Bottom Right Of The Quad (Bottom)

				glVertex3f( size, size, size);	// Top Right Of The Quad (Front)
				glVertex3f(-size, size, size);	// Top Left Of The Quad (Front)
				glVertex3f(-size,-size, size);	// Bottom Left Of The Quad (Front)
				glVertex3f( size,-size, size);	// Bottom Right Of The Quad (Front)

				glVertex3f( size,-size,-size);	// Top Right Of The Quad (Back)
				glVertex3f(-size,-size,-size);	// Top Left Of The Quad (Back)
				glVertex3f(-size, size,-size);	// Bottom Left Of The Quad (Back)
				glVertex3f( size, size,-size);	// Bottom Right Of The Quad (Back)

				glVertex3f(-size, size, size);	// Top Right Of The Quad (Left)
				glVertex3f(-size, size,-size);	// Top Left Of The Quad (Left)
				glVertex3f(-size,-size,-size);	// Bottom Left Of The Quad (Left)
				glVertex3f(-size,-size, size);	// Bottom Right Of The Quad (Left)

				glVertex3f( size, size,-size);	// Top Right Of The Quad (Right)
				glVertex3f( size, size, size);	// Top Left Of The Quad (Right)
				glVertex3f( size,-size, size);	// Bottom Left Of The Quad (Right)
				glVertex3f( size,-size,-size);	// Bottom Right Of The Quad (Right)
			glEnd();			// End Drawing The Cube
		}
	}
}
///////////////////////////////// RENDER SCENE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*


void RenderScene() 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();									// Reset The matrix

	g_Camera.Look();


	//light1
	float l1_amb[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	float l1_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float l1_spe[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float l1_pos[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	glLightfv(GL_LIGHT1, GL_AMBIENT, l1_amb);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, l1_dif);
	glLightfv(GL_LIGHT1, GL_SPECULAR, l1_spe);
	glLightfv(GL_LIGHT1, GL_POSITION, l1_pos);

	//light0
	float l0_amb[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	float l0_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float l0_spe[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	float l0_pos[] = { g_Camera.m_vPosition.x, g_Camera.m_vPosition.y, g_Camera.m_vPosition.z, 1.0f };

	float temp = 0.0f, temp2 = 0.0f, temp3 = 0.0f;

	if(g_Camera.m_vView.z < g_Camera.m_vPosition.z)
	{
		temp = g_Camera.m_vView.x - g_Camera.m_vPosition.x;
		temp2 = g_Camera.m_vView.z - g_Camera.m_vPosition.z;
	}
	else
	{
		temp = g_Camera.m_vView.x - g_Camera.m_vPosition.x;
		temp2 = g_Camera.m_vView.z - g_Camera.m_vPosition.z;
	}

	float l0_pos1[] = {temp, 0.0f, temp2};
	//float l0_pos1[] = {-1.0f, 0.0f, -1.0f};
	
	glLightfv(GL_LIGHT0, GL_AMBIENT, l0_amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l0_dif);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l0_spe);
	glLightfv(GL_LIGHT0, GL_POSITION, l0_pos);

	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 15.0f);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, l0_pos1);

	// Draw a grid so we can get a good idea of movement around the world
	float m1_amb[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float m1_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float m1_spe[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, m1_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m1_dif);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m1_spe);
	glMaterialf(GL_FRONT, GL_SHININESS, 0.0f);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	Draw3DSGrid();

	glPushMatrix();
	DrawCube();
	glPopMatrix();
	// Draw the pyramids that spiral in to the center
	//DrawSpiralTowers();



	// Swap the backbuffers to the foreground
	SwapBuffers(g_hDC);
}

///////////////////////////////// WIN PROC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

LRESULT CALLBACK WinProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    LONG    lRet = 0; 
    PAINTSTRUCT    ps;

    switch (uMsg)
	{ 
    case WM_SIZE:										// If the window is resized
		if(!g_bFullScreen)								// Do this only if we are in window mode
		{
			SizeOpenGLScreen(LOWORD(lParam),HIWORD(lParam));// LoWord=Width, HiWord=Height
			GetClientRect(hWnd, &g_rRect);					// Get the window rectangle
		}
        break; 
 
	case WM_PAINT:										// If we need to repaint the scene
		BeginPaint(hWnd, &ps);							// Init the paint struct		
		EndPaint(hWnd, &ps);							// EndPaint, Clean up
		break;

	case WM_KEYDOWN:

		switch(wParam) {								// Check if we hit a key
			case VK_ESCAPE:								// If we hit the escape key
				PostQuitMessage(0);						// Send a QUIT message to the window
				break;
		}
		break;

    case WM_CLOSE:										// If the window is being closes
        PostQuitMessage(0);								// Send a QUIT Message to the window
        break; 
     
    default:											// Return by default
        lRet = DefWindowProc (hWnd, uMsg, wParam, lParam); 
        break; 
    } 
 
    return lRet;										// Return by default

}